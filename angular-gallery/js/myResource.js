(function(){
    angular
        .module('app')
        .factory('galleryResource',galleryResource)

    galleryResource.$inject = ['$resource'];

function galleryResource($resource) {
    return $resource('http://jsonplaceholder.typicode.com/photos')
}
})();