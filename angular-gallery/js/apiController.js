(function(){
    angular
        .module('app')
        .controller('apiController' , apiController)
        .directive('myDirective' , myDirective);


    apiController.$inject = ['galleryResource','Gallery'];

    function apiController(galleryResource,Gallery){
        var vm = this;
    /*
        //-----------    $resource
            vm.images = galleryResource.query(function(){
            console.log(vm.images);
            });

    */
        vm.images = [];
        vm.quantity = 15;
        vm.getImages = getImages;
        activate();
        function activate() {
            return getImages().then(function() {
                console.log(vm.images);
            });
        }
        function getImages(){
            return Gallery.getImages().then(function(data){
                vm.images = data;
                return vm.images
            });
        }
    }

    function myDirective(){

        return {
            replace: true,
            scope: {
                image: "="
            },
            restrict: "A",
            template: "<div><img ng-src='{{image.thumbnailUrl}}'><div>{{image.title}}</div></div>",
            link: function(scope,element,attrs){
                var elem = element[0].addEventListener("click", function (){
                    var slide = document.createElement('div');
                    var hugePict = document.createElement('img');
                    document.body.style.overflow = "hidden";
                    document.body.appendChild(slide);
                    hugePict.src = scope.image.url;
                    hugePict.className = 'big-picture';
                    slide.className = 'wrap';
                    slide.appendChild(hugePict);
                    slide.addEventListener("click", function (){
                        document.body.style.overflow = "scroll"
                        slide.parentNode.removeChild(slide);
                    });
                });
            }

        }
    }

})();