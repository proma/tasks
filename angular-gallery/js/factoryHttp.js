(function() {
    angular
        .module('app')
        .factory('Gallery', Gallery);

    Gallery.$inject = ['$http'];

    function Gallery($http ) {
        return {
            getImages: getImages
        };
        function getImages() {
            return $http.get('http://jsonplaceholder.typicode.com/photos')
                .then(getImagesComplete)
                .catch(getImagesFailed);

            function getImagesComplete(res) {
             return res.data;
            }

            function getImagesFailed() {
                console.log('error');
            }

        }

    }
})();