var winston = require('winston');
var ENV = process.env.NODE_ENV;

function getLogger(module) {
    var PATH_SEPARATOR = require('path').sep;
    var path = module.filename.split(PATH_SEPARATOR).slice(-2).join(PATH_SEPARATOR);

    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize : true,
                level : ENV == 'development' ? 'debug' : 'error',
                label : path
            })
            ]
    })

}
module.exports = getLogger;