var main = function () {
    $('.add-goods').keypress(function(e) {
        if (e.which == 13) {

            var post = $('.add-goods').val();
            $('<li class="list">' +
            '<input type="checkbox" class="goods-check"   /> ' +
            '<input type="text" class="field"  value="' + post + ' " readonly="true" />' +
            '<button type="submit" class="remove-button"><img id="remove-button-img" src="close.PNG" alt="delete"/></button>' +
            '</li>').fadeIn('slow').appendTo('.goods');
            $('.add-goods').val('');
            $('.remove-button').hide();
        }
    });

    $('.delete-all').on('click',function(){
        $('li input:checked').parents('li').remove();
    });

    $('body').on('dblclick','.field',function(){
        var oldValue= $(this).val();
         $(this).prop('readonly',false).focus();
         $(this).keydown(function(e) {
            if(e.which == 13) {
             $(this).prop('readonly',true);
            } else if(e.which == 27){
                 $(this).val(oldValue);
                 $(this).prop('readonly',true);
            };
         });
    });
    $('ul').on('click','.remove-button',function(){
        $(this).parents('li').remove();

    });
    $('ul').on('change','.goods-check',function() {

        if ($(this).prop('checked')) {
            $(this).next().css( {'text-decoration' : 'line-through',
                                                    'opacity': '0.5'});
        } else {
            $(this).next().css({'text-decoration' : 'none',
                                                    'opacity': '1'});
        };
    });

    $('ul').on('change','.check-all',function() {

       if ($(this).prop('checked')) {
            $('.goods-check').prop('checked', true);
         } else {
           $('.goods-check').prop('checked', false);
       };

        $('.goods-check').trigger('change');
    });


    $('ul').on('mouseenter', '.list', function () {
        $(this).find(":button").show();
    }).on('mouseleave', '.list', function () {
        $(this).find(":button").hide();
    });

};
$(document).ready(main);

